package carware;

import apoio.ConexaoBD;
import javax.swing.JOptionPane;
import tela.DlgLogin;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class Carware {

    public static void main(String[] args) {
        setLookAndFeel();
        conectarAoBanco();
    }

    public static void setLookAndFeel() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public static void conectarAoBanco() {
        if (ConexaoBD.getInstance().getConnection() != null) {
            DlgLogin login = new DlgLogin(null, true);
            login.setLocationRelativeTo(null);
            login.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Deu problema!");
        }
    }
}
