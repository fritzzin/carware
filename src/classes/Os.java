/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "os")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Os.findAll", query = "SELECT o FROM Os o")
    , @NamedQuery(name = "Os.findById", query = "SELECT o FROM Os o WHERE o.id = :id")
    , @NamedQuery(name = "Os.findByDataentrada", query = "SELECT o FROM Os o WHERE o.dataentrada = :dataentrada")
    , @NamedQuery(name = "Os.findByDatatermino", query = "SELECT o FROM Os o WHERE o.datatermino = :datatermino")
    , @NamedQuery(name = "Os.findByDatasaida", query = "SELECT o FROM Os o WHERE o.datasaida = :datasaida")
    , @NamedQuery(name = "Os.findByValortotalproduto", query = "SELECT o FROM Os o WHERE o.valortotalproduto = :valortotalproduto")
    , @NamedQuery(name = "Os.findByValormaoobra", query = "SELECT o FROM Os o WHERE o.valormaoobra = :valormaoobra")
    , @NamedQuery(name = "Os.findByProblemacliente", query = "SELECT o FROM Os o WHERE o.problemacliente = :problemacliente")
    , @NamedQuery(name = "Os.findByProblemamecanico", query = "SELECT o FROM Os o WHERE o.problemamecanico = :problemamecanico")
    , @NamedQuery(name = "Os.findByAvaliacaoveiculo", query = "SELECT o FROM Os o WHERE o.avaliacaoveiculo = :avaliacaoveiculo")
    , @NamedQuery(name = "Os.findByStatus", query = "SELECT o FROM Os o WHERE o.status = :status")})
public class Os implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "dataentrada")
    @Temporal(TemporalType.DATE)
    private Date dataentrada;
    @Column(name = "datatermino")
    @Temporal(TemporalType.DATE)
    private Date datatermino;
    @Column(name = "datasaida")
    @Temporal(TemporalType.DATE)
    private Date datasaida;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valortotalproduto")
    private BigDecimal valortotalproduto;
    @Column(name = "valormaoobra")
    private BigDecimal valormaoobra;
    @Basic(optional = false)
    @Column(name = "problemacliente")
    private String problemacliente;
    @Column(name = "problemamecanico")
    private String problemamecanico;
    @Column(name = "avaliacaoveiculo")
    private String avaliacaoveiculo;
    @Basic(optional = false)
    @Column(name = "status")
    private Character status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idos")
    private Collection<Maoobra> maoobraCollection;
    @JoinColumn(name = "idveiculopessoa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Veiculopessoa idveiculopessoa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idos")
    private Collection<Osservicosbasicos> osservicosbasicosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idos")
    private Collection<Produtoos> produtoosCollection;

    public Os() {
    }

    public Os(Integer id) {
        this.id = id;
    }

    public Os(Integer id, Date dataentrada, String problemacliente, Character status) {
        this.id = id;
        this.dataentrada = dataentrada;
        this.problemacliente = problemacliente;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataentrada() {
        return dataentrada;
    }

    public void setDataentrada(Date dataentrada) {
        this.dataentrada = dataentrada;
    }

    public Date getDatatermino() {
        return datatermino;
    }

    public void setDatatermino(Date datatermino) {
        this.datatermino = datatermino;
    }

    public Date getDatasaida() {
        return datasaida;
    }

    public void setDatasaida(Date datasaida) {
        this.datasaida = datasaida;
    }

    public BigDecimal getValortotalproduto() {
        return valortotalproduto;
    }

    public void setValortotalproduto(BigDecimal valortotalproduto) {
        this.valortotalproduto = valortotalproduto;
    }

    public BigDecimal getValormaoobra() {
        return valormaoobra;
    }

    public void setValormaoobra(BigDecimal valormaoobra) {
        this.valormaoobra = valormaoobra;
    }

    public String getProblemacliente() {
        return problemacliente;
    }

    public void setProblemacliente(String problemacliente) {
        this.problemacliente = problemacliente;
    }

    public String getProblemamecanico() {
        return problemamecanico;
    }

    public void setProblemamecanico(String problemamecanico) {
        this.problemamecanico = problemamecanico;
    }

    public String getAvaliacaoveiculo() {
        return avaliacaoveiculo;
    }

    public void setAvaliacaoveiculo(String avaliacaoveiculo) {
        this.avaliacaoveiculo = avaliacaoveiculo;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<Maoobra> getMaoobraCollection() {
        return maoobraCollection;
    }

    public void setMaoobraCollection(Collection<Maoobra> maoobraCollection) {
        this.maoobraCollection = maoobraCollection;
    }

    public Veiculopessoa getIdveiculopessoa() {
        return idveiculopessoa;
    }

    public void setIdveiculopessoa(Veiculopessoa idveiculopessoa) {
        this.idveiculopessoa = idveiculopessoa;
    }

    @XmlTransient
    public Collection<Osservicosbasicos> getOsservicosbasicosCollection() {
        return osservicosbasicosCollection;
    }

    public void setOsservicosbasicosCollection(Collection<Osservicosbasicos> osservicosbasicosCollection) {
        this.osservicosbasicosCollection = osservicosbasicosCollection;
    }

    @XmlTransient
    public Collection<Produtoos> getProdutoosCollection() {
        return produtoosCollection;
    }

    public void setProdutoosCollection(Collection<Produtoos> produtoosCollection) {
        this.produtoosCollection = produtoosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Os)) {
            return false;
        }
        Os other = (Os) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Os[ id=" + id + " ]";
    }
    
}
