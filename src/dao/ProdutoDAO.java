package dao;

import classes.Produto;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JTable;

/**
 *
 * @author Augusto Fritz
 */
public class ProdutoDAO extends GenericDAO {

    private EntityManager em;

    public List<Produto> popularTabela(JTable tabela) {
        Query query = em.createQuery("select * from produto p");
        return query.getResultList();
    }
}