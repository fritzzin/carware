/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import apoio.ConexaoBD;
import apoio.IDAO_T;
import classes.Funcionario;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author lucas.assis
 */
public class FuncionarioDAO implements IDAO_T<Funcionario> {

    ResultSet resultadoQ = null;

    @Override
    public String salvar(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "INSERT INTO funcionario VALUES ("
                    + " " + o.getIdpessoa() + ","
                    + "'" + o.getIdfuncao() + "',"
                    + "'" + o.getIdlogin() + "',"
                    + "'" + o.getCtps() + "',"
                    + "'" + o.getSalario() + "',"
                    + "'" + o.getEspecialidade() + "',"
                    + "" + o.getDataadmissao() + ""
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro salvar pessoa = " + e);
            return e.toString();
        }
    }

    @Override
    public String atualizar(Funcionario o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "UPDATE funcionario "
                    + "SET "
                    + "idFuncao = '" + o.getIdfuncao()+ "',"
                    + "idLogin = '" + o.getIdlogin()+ "',"
                    + "ctps = '" + o.getCtps() + "', "
                    + "salario = '" + o.getSalario() + "', "
                    + "especialidade = '" + o.getEspecialidade() + "', "
                    + "dataAdmissao = '" + o.getDataadmissao()+ "' "
                    + "WHERE pessoa_idpessoa = " + o.getIdpessoa();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro atualizar pessoa = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Funcionario> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Funcionario> consultar(String criterio) {
        return null;

    }

    //Refazer isso aqui
    @Override
    public Funcionario consultarId(int id) {
        Funcionario f = new Funcionario();
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "select f.pessoa_idpessoa, f.num_carteira_trabalho, f.especialidade, f.login, f.senha, f.status\n"
                    + "from funcionario f\n"
                    + "where f.pessoa_idpessoa = " + id;

            System.out.println("sql: " + sql);

            resultadoQ = st.executeQuery(sql);
            /*
            if (resultadoQ.next()) {
                f.setEspecialidade(resultadoQ.getString("especialidade"));
                f.setIdLogin(resultadoQ.getString("idLogin"));
                f.setNum_carteira_trabalho(resultadoQ.getString("num_carteira_trabalho"));
                f.setPessoa_idpessoa(resultadoQ.getInt("pessoa_idpessoa"));
                f.setStatus(resultadoQ.getBoolean("status"));
                f.setSenha(resultadoQ.getString("senha"));
            }*/

        } catch (Exception e) {
            System.out.println("Erro consultar pessoa = " + e);
        }
        return f;
    }

    public void popularTabelaBusca(JTable tabela, String criterio) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[3];
        cabecalho[0] = "Id";
        cabecalho[1] = "Nome";
        cabecalho[2] = "Especialidade";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select count(*) \n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and p.nome ILIKE '%" + criterio + "%'");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][cabecalho.length];

        } catch (Exception e) {
            System.out.println("Erro ao consultar pessoa: " + e);
            System.out.println(""
                    + "select count(*) \n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and p.nome ILIKE '%" + criterio + "%'");
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select f.pessoa_idpessoa, p.nome, f.especialidade  \n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and p.nome ILIKE '%" + criterio + "%'\n"
                    + "order by p.nome");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("pessoa_idpessoa");
                dadosTabela[lin][1] = resultadoQ.getString("nome");
                dadosTabela[lin][2] = resultadoQ.getString("especialidade");

                // caso a coluna precise exibir uma imagem
//                if (resultadoQ.getBoolean("Situacao")) {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_ativo.png"));
//                } else {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_inativo.png"));
//                }
                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
            System.out.println(""
                    + "select f.pessoa_idpessoa, p.nome, f.especialidade  \n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and p.nome ILIKE '%" + criterio + "%'\n"
                    + "and order by p.nome");
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
                /*  
                 if (column == 3) {  // apenas a coluna 3 sera editavel
                 return true;
                 } else {
                 return false;
                 }
                 */
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
        // renderizacao das linhas da tabela = mudar a cor
//        jTable1.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
//
//            @Override
//            public Component getTableCellRendererComponent(JTable table, Object value,
//                    boolean isSelected, boolean hasFocus, int row, int column) {
//                super.getTableCellRendererComponent(table, value, isSelected,
//                        hasFocus, row, column);
//                if (row % 2 == 0) {
//                    setBackground(Color.GREEN);
//                } else {
//                    setBackground(Color.LIGHT_GRAY);
//                }
//                return this;
//            }
//        });
    }

    //input exemplo = (this, lucas, p.nome);
    //Ajeitar isso aq dps
    public void popularTabela(JTable tabela, String criterio, String tipo) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[8];
        cabecalho[0] = "Id";
        cabecalho[1] = "Nome";
        cabecalho[2] = "Especialidade";
        cabecalho[3] = "Ctps";
        cabecalho[4] = "Login";
        cabecalho[5] = "Salário";
        cabecalho[6] = "Data Admissao";
        cabecalho[7] = "Função";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select count(*) \n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][cabecalho.length];

        } catch (Exception e) {
            System.out.println("Erro ao consultar pessoa: " + e);
            System.out.println(""
                    + "select count(*) \n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and p.nome ILIKE '%" + criterio + "%'");
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select f.pessoa_idpessoa, p.nome, f.especialidade, f.num_carteira_trabalho, f.login, f.status\n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and p.nome ILIKE '%" + criterio + "%'\n"
                    + "order by f.status DESC ,p.nome ");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("pessoa_idpessoa");
                dadosTabela[lin][1] = resultadoQ.getString("nome");
                dadosTabela[lin][2] = resultadoQ.getString("especialidade");
                dadosTabela[lin][3] = resultadoQ.getString("num_carteira_trabalho");
                dadosTabela[lin][4] = resultadoQ.getString("login");
                if (resultadoQ.getBoolean("status")) {
                    dadosTabela[lin][5] = "Ativo";
                } else {
                    dadosTabela[lin][5] = "Inativo";
                }

                // caso a coluna precise exibir uma imagem
//                if (resultadoQ.getBoolean("Situacao")) {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_ativo.png"));
//                } else {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_inativo.png"));
//                }
                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
            System.out.println(""
                    + "select f.pessoa_idpessoa, p.nome, f.especialidade  \n"
                    + "from funcionario f, pessoa p\n"
                    + "where f.pessoa_idpessoa = p.idpessoa\n"
                    + "and p.nome ILIKE '%" + criterio + "%'\n"
                    + "and order by p.nome");
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
                /*  
                 if (column == 3) {  // apenas a coluna 3 sera editavel
                 return true;
                 } else {
                 return false;
                 }
                 */
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
        // renderizacao das linhas da tabela = mudar a cor
//        jTable1.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
//
//            @Override
//            public Component getTableCellRendererComponent(JTable table, Object value,
//                    boolean isSelected, boolean hasFocus, int row, int column) {
//                super.getTableCellRendererComponent(table, value, isSelected,
//                        hasFocus, row, column);
//                if (row % 2 == 0) {
//                    setBackground(Color.GREEN);
//                } else {
//                    setBackground(Color.LIGHT_GRAY);
//                }
//                return this;
//            }
//        });
    }

}
