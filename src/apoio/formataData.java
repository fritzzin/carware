/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apoio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author lucas.assis
 */
public class formataData {

    public static String formatarData(String dataN) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date data;
        try {
            data = formato.parse(dataN);
            formato.applyPattern("dd/MM/yyyy");
            String dataFormatada = formato.format(data);
            return dataFormatada;
        } catch (ParseException ex) {
            return ex.toString();
        }
    }
}
